import lowdb from "lowdb";
import FileSync from "lowdb/adapters/FileSync";
import { Data } from "../types/models/index";
import config from "./env";

let db: lowdb.LowdbSync<Data>;

export const createConnection = async () => {
  const defaultData: Data = { users: [] };
  const adapter = new FileSync<Data>(config.db.file);
  db = lowdb(adapter);
  db.defaults(defaultData).write();
};

export const getConnection = () => db;
