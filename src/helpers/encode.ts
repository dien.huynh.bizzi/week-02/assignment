import * as bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import config from "../config/env";
import { tokenEnum } from "../constants/enum";

export const createToken = (userId: string, tokenKey?: tokenEnum): string => {
  if (tokenKey === tokenEnum.RefreshToken) {
    const token = jwt.sign({}, config.jwt.refreshToken as string, {
      expiresIn: 3600 * +config.jwt.refreshTokenExpiresin,
      audience: String(userId),
    });
    return token;
  }

  if (tokenKey === tokenEnum.ResetPasswordToken) {
    const token = jwt.sign({}, config.jwt.resetPasswordToken as string, {
      expiresIn: 3600 * +config.jwt.resetPasswordTokenExpiresin,
      audience: String(userId),
    });
    return token;
  }

  const token = jwt.sign({}, config.jwt.accessToken as string, {
    expiresIn: 3600 * +config.jwt.accessTokenExpiresin,
    audience: String(userId),
  });
  return token;
};

export const compareString = async (string: string, hash: string): Promise<boolean> => {
  const token = await bcrypt.compare(string, hash);
  return token;
};

export const hashString = async (string: string): Promise<string> => {
  const token = await bcrypt.hash(string, 10);
  return token;
};
