import { authResolvers, authTypeDefs } from "./auth/index";

export const resolvers = {
  Query: {
    ...authResolvers.Query,
  },
  Mutation: {
    ...authResolvers.Mutation,
  },
};

export const typeDefs = [authTypeDefs];
