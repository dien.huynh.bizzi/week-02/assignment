import { authResolvers } from "./resolvers/index";
import { authTypeDefs } from "./typeDefs/index";

export { authResolvers, authTypeDefs };
