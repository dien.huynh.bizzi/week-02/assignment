export const RegisterTypeDefs = `#graphql
  input RegisterInput {
    username: String!,
    password: String!,
    email: String!,
  }
  
  type RegisterOutput {
    msg: String!
    id: String!
    username: String!
  }
`;
