export const LogoutTypeDefs = `#graphql
  type LogoutOutput {
    msg: String!
  }
`;
