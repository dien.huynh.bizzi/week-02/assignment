export const ResetPasswordTypeDefs = `#graphql
  input ResetPasswordInput {
    jwtResetPass: String!, 
    password: String!
  }

  type ResetPasswordOutput {
    msg: String!
  }
`;
