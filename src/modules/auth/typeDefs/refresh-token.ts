export const RefreshTokenTypeDefs = `#graphql
  input RefreshTokenInput {
    jwtRefesh: String!, 
  }

  type RefreshTokenOutput {
    msg: String!
    AccessToken: String!,
    RefreshToken: String!,
  }
`;
