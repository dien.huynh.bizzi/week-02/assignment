import { UserTypeDefs } from "../../../types/TypeDefs";
import { ForgotPasswordTypeDefs } from "./forgot-password";
import { LoginTypeDefs } from "./login";
import { LogoutTypeDefs } from "./logout";
import { RefreshTokenTypeDefs } from "./refresh-token";
import { RegisterTypeDefs } from "./register";
import { ResetPasswordTypeDefs } from "./reset-password";

export const authTypeDefs = [
  UserTypeDefs,
  `#graphql
  type Query {
    users: [User]
    user(id: ID!): User
    helloWorld: String
  }

  type Mutation {
    register(user: RegisterInput!): RegisterOutput
    login(user: LoginInput!): LoginOutput
    logout: LogoutOutput
    forgotPassword(user: ForgotPasswordInput!): ForgotPasswordOutput
    resetPassword(user: ResetPasswordInput!): ResetPasswordOutput
    refresh(user: RefreshTokenInput!): RefreshTokenOutput
  }
`,
  RegisterTypeDefs,
  LoginTypeDefs,
  LogoutTypeDefs,
  ForgotPasswordTypeDefs,
  RefreshTokenTypeDefs,
  ResetPasswordTypeDefs,
];
