export const LoginTypeDefs = `#graphql
  input LoginInput {
    username: String!,
    password: String!,
    isRemember: Boolean!
  }

  type LoginOutput {
    msg: String!
    id: String!
    username: String!
    accessToken: String!
    refreshToken: String!
  }
`;
