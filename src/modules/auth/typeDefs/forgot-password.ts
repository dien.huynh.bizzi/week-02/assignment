export const ForgotPasswordTypeDefs = `#graphql
  input ForgotPasswordInput {
    email: String!,
  }

  type ForgotPasswordOutput {
    msg: String!
  }
`;
