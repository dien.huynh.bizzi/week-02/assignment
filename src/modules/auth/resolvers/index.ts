import { getConnection } from "../../../config/db";
import { forgotPassword } from "./forgot-password";
import { login } from "./login";
import { logout } from "./logout";
import { refresh } from "./refresh-token";
import { register } from "./register";
import { resetPassword } from "./reset-password";

export const authResolvers = {
  Query: {
    users: (parent: any, args: any, contextValue: any, info: any) => {
      return getConnection().get("users").value();
    },
    user: (parent: any, args: any, contextValue: any, info: any) => {
      return getConnection().get("users").find({ id: args.id }).value();
    },
  },
  Mutation: {
    register,
    login,
    logout,
    forgotPassword,
    resetPassword,
    refresh
  },
};
