import { getConnection } from "../../../config/db";
import { MSG_ERR_USERNAME_WAS_EXIST, MSG_REGISTER_SUCCESSFULLY } from "../../../constants/message";
import { hashString } from "../../../helpers/encode";
import { v4 as uuidv4 } from "uuid";
import { User } from "../../../types/models/user";

export const register = async (parent: any, args: any, contextValue: any, info: any) => {
  const { username, password, email } = args.user;

  const foundUser = getConnection().get("users").find({ username }).value();

  if (foundUser) {
    throw new Error(MSG_ERR_USERNAME_WAS_EXIST);
  }

  const hashPassword = await hashString(password);

  const user: User = {
    id: uuidv4(),
    username,
    password: hashPassword,
    email,
    refreshToken: null,
    resetPass: null,
  };

  getConnection().get("users").push(user).write();

  return { msg: MSG_REGISTER_SUCCESSFULLY, id: user.id, username: user.username };
};
