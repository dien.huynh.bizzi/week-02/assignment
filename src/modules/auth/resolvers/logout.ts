import { getConnection } from "../../../config/db";
import { MSG_ERR_INVALID_TOKEN, MSG_ERR_USER_IS_NOT_EXIST, MSG_LOGOUT_SUCCESSFULLY } from "../../../constants/message";

export const logout = async (parent: any, args: any, contextValue: any, info: any) => {
  const { aud } = contextValue.user;

  if (!aud) {
    throw new Error(MSG_ERR_INVALID_TOKEN);
  }

  const id = aud;

  getConnection().read();
  const foundUser = getConnection().get("users").find({ id }).value();

  if (!foundUser) {
    throw new Error(MSG_ERR_USER_IS_NOT_EXIST);
  }

  if (foundUser?.refreshToken !== null) {
    getConnection().get("users").find({ id: foundUser.id }).assign({ refreshToken: null }).write();
  }

  return { msg: MSG_LOGOUT_SUCCESSFULLY };
};
