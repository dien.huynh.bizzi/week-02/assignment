import { getConnection } from "../../../config/db";
import config from "../../../config/env";
import {
  MSG_ERR_CREDENTIAL_IS_INCORRECT,
  MSG_ERR_INVALID_TOKEN,
  MSG_ERR_USER_IS_NOT_EXIST,
  MSG_RESET_PASS_SUCCESSFULLY,
} from "../../../constants/message";
import { compareString, hashString } from "../../../helpers/encode";
import jwt from "jsonwebtoken";

export const resetPassword = async (parent: any, args: any, contextValue: any, info: any) => {
  const { jwtResetPass, password } = args.user;

  let verify: any;
  try {
    verify = jwt.verify(jwtResetPass, config.jwt.resetPasswordToken as string);
  } catch (err) {
    throw new Error(MSG_ERR_INVALID_TOKEN);
  }

  const foundUser = getConnection().get("users").find({ id: verify.aud }).value();

  if (!foundUser) {
    throw new Error(MSG_ERR_USER_IS_NOT_EXIST);
  }

  const jwtResetPassSlice = jwtResetPass.slice(jwtResetPass.lastIndexOf("."));

  const isMatch = await compareString(jwtResetPassSlice, foundUser.resetPass || "");

  if (!isMatch) {
    throw new Error(MSG_ERR_CREDENTIAL_IS_INCORRECT);
  }
  const hashPassword = await hashString(password);

  getConnection()
    .get("users")
    .find({ id: foundUser.id })
    .assign({ password: hashPassword, resetPassword: null })
    .write();

  return { msg: MSG_RESET_PASS_SUCCESSFULLY };
};
