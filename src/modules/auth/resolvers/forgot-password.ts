import { getConnection } from "../../../config/db";
import config from "../../../config/env";
import { tokenEnum } from "../../../constants/enum";
import { MSG_ERR_EMAIL_IS_NOT_EXIST, MSG_SEND_RESET_PASS_EMAIL_SUCCESSFULLY } from "../../../constants/message";
import { createToken, hashString } from "../../../helpers/encode";
import { resetPassMail } from "../../mails/templates/resetPassword";

export const forgotPassword = async (parent: any, args: any, contextValue: any, info: any) => {
  const { email } = args.user;

  getConnection().read();
  const foundUser = getConnection().get("users").find({ email }).value();

  if (!foundUser) {
    throw new Error(MSG_ERR_EMAIL_IS_NOT_EXIST);
  }

  const jwtResetToken = createToken(foundUser.id, tokenEnum.ResetPasswordToken);
  const newjwtResetToken = jwtResetToken.slice(jwtResetToken.lastIndexOf("."));
  const hash = await hashString(newjwtResetToken);

  getConnection().get("users").find({ id: foundUser.id }).assign({ resetPass: hash }).write();

  resetPassMail({
    email: foundUser.email,
    url: `${config.client.host}/reset-password?jwt=${jwtResetToken}`,
  });

  return { msg: MSG_SEND_RESET_PASS_EMAIL_SUCCESSFULLY };
};
