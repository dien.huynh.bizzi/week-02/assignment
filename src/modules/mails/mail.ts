import nodeMailer from "nodemailer";
import config from "../../config/env";

export const sendEMail = (to: any, subject: any, htmlContent: any) => {
  const transporter = nodeMailer.createTransport({
    host: config.mail.host,
    port: +config.mail.port,
    auth: {
      user: config.mail.user,
      pass: config.mail.password,
    },
  });

  const mailOptions = {
    from: config.mail.mailFrom,
    to: to,
    subject: subject,
    html: htmlContent,
  };

  return transporter.sendMail(mailOptions, function (error, info) {
    if (error) {
      console.log(error);
    } else {
      console.log("Email sent: " + info.response);
    }
  });
};
