export const UserTypeDefs = `#graphql
  type User {
    id: ID!
    username: String!
    password: String!
    email: String!
    refreshToken: String
    resetPass: String
  }
`;
