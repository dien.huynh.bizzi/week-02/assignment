import fastify, { FastifyInstance, FastifyServerOptions } from "fastify";
import { ApolloServer } from "@apollo/server";
import { fastifyApolloHandler, fastifyApolloDrainPlugin } from "@as-integrations/fastify";
import { resolvers } from "./modules/index";
import { typeDefs } from "./modules/index";
import { createConnection } from "./config/db";
import { AuthContext, authContextFunction } from "./contexts/auth-context";

const App = (options: FastifyServerOptions) => {
  const app = fastify(options);

  const apollo = new ApolloServer<AuthContext>({
    typeDefs,
    resolvers,
    plugins: [fastifyApolloDrainPlugin(app)],
  });

  (async () => {
    await createConnection();
    await apollo.start();
  })();

  app.register(async (app: FastifyInstance) => {
    app.route({
      url: "/graphql",
      method: ["POST", "OPTIONS"],
      handler: fastifyApolloHandler(apollo, {
        context: authContextFunction,
      }),
    });
  });

  return app;
};
export default App;
