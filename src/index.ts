import { FastifyInstance, FastifyServerOptions } from "fastify";
import App from "./app";
import config from "./config/env";

const options: FastifyServerOptions = {
  logger: true,
};

const app: FastifyInstance = App(options);

const PORT: string | number = +config.port;

app.listen({ port: PORT }, (err, address) => {
  if (err) {
    console.error(err);
    process.exit(1);
  }
  console.log(`Server listening at ${address}`);
});

export default app;
