import { afterAll, beforeAll, describe, expect, jest, test } from "@jest/globals";
import app from "../index";
import { FastifyInstance } from "fastify";
import { UserData } from "./data/user";
import { getConnection } from "../config/db";
import { User } from "../types/models/user";
import { compareString, createToken } from "../helpers/encode";

const helpers = {
  compareString,
  createToken,
};
const db = {
  async create(user: User): Promise<User> {
    getConnection()
      .get("users")
      .push({ ...user })
      .write();

    return user;
  },
  async find(expression: (user: User) => boolean): Promise<User | undefined> {
    const user = getConnection().get("users");
    const index = user.findIndex(expression).value();
    if (index === -1) {
      return undefined;
    }
    const result = user.get(index).value();
    return { ...result };
  },
};

describe("[TEST] Auth", () => {
  let fastify: FastifyInstance;
  beforeAll(async () => {
    fastify = app;
  });

  afterAll(() => {
    fastify.close();
  });

  describe("[TEST] /graphql register", () => {
    const RegisterMutation = `
      mutation RegisterMutation($user: RegisterInput!){
        register(user: $user){
            msg,
            id,
            username
        }
      }
    `;
    const RegisterInput = {
      user: {
        username: UserData.username,
        password: UserData.password,
        email: UserData.email,
      },
    };

    test("Testing username was exist", async () => {
      jest.spyOn(db, "find").mockImplementation(() => Promise.resolve({ ...UserData }));

      const response = await fastify.inject({
        method: "POST",
        url: "/graphql",
        body: {
          query: RegisterMutation,
          variables: {
            user: {
              username: "thanhdien1",
              password: UserData.password,
              email: UserData.email,
            },
          },
        },
      });

      expect(response.statusCode).toBe(200);
      const json = response.json();
      expect(json.data.register).toBeNull();
      expect(json.errors).toBeTruthy();
    });

    test("Testing register successfully", async () => {
      jest.spyOn(db, "find").mockImplementation(() => Promise.resolve(undefined));
      jest.spyOn(db, "create").mockImplementation(() => Promise.resolve({ ...UserData }));

      const response = await fastify.inject({
        method: "POST",
        url: "/graphql",
        body: {
          query: RegisterMutation,
          variables: {
            ...RegisterInput,
          },
        },
      });

      expect(response.statusCode).toBe(200);
      const json = response.json();
      expect(json.data.register).toBeTruthy();
      expect(json.data.register.username).toBe(UserData.username);
    });
  });

  describe("[TEST] /graphql login", () => {
    const LoginMutation = `
      mutation LoginMutation($user: LoginInput!){
        login(user: $user){
            msg
            id
            username
            accessToken
            refreshToken
        }
    }
      `;
    const LoginInput = {
      user: {
        username: UserData.username,
        password: UserData.password,
        isRemember: true,
      },
    };

    test("Testing user is not exist", async () => {
      jest.spyOn(db, "find").mockImplementation(() => Promise.resolve(undefined));

      const response = await fastify.inject({
        method: "POST",
        url: "/graphql",
        body: {
          query: LoginMutation,
          variables: {
            user: {
              username: "khongcotaikhoan",
              password: UserData.password,
              isRemember: true,
            },
          },
        },
      });

      expect(response.statusCode).toBe(200);
      const json = response.json();
      expect(json.data.login).toBeNull();
      expect(json.errors).toBeTruthy();
    });

    test("Testing your credential is incorrect", async () => {
      jest.spyOn(db, "find").mockImplementation(() => Promise.resolve({ ...UserData }));
      jest.spyOn(helpers, "compareString").mockResolvedValue(false);

      const response = await fastify.inject({
        method: "POST",
        url: "/graphql",
        body: {
          query: LoginMutation,
          variables: {
            user: {
              username: "thanhdien1",
              password: "matkhausai",
              isRemember: true,
            },
          },
        },
      });

      expect(response.statusCode).toBe(200);
      const json = response.json();
      expect(json.data.login).toBeNull();
      expect(json.errors).toBeTruthy();
    });

    test("Testing login successfully", async () => {
      jest.spyOn(db, "find").mockImplementation(() => Promise.resolve({ ...UserData }));
      jest.spyOn(helpers, "compareString").mockResolvedValue(true);
      jest.spyOn(helpers, "createToken").mockReturnValue("");

      const response = await fastify.inject({
        method: "POST",
        url: "/graphql",
        body: {
          query: LoginMutation,
          variables: {
            user: {
              username: "thanhdien1",
              password: "thanhdien2",
              isRemember: true,
            },
          },
        },
      });

      expect(response.statusCode).toBe(200);
      const json = response.json();
      expect(json.data.login).toBeTruthy();
      expect(json.data.login.username).toEqual("thanhdien1");
    });
  });
});
