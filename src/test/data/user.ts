import { faker } from "@faker-js/faker";

export const UserData = {
  id: faker.string.sample(),
  username: faker.string.sample(),
  password: faker.string.sample(),
  email: faker.string.sample(),
  refreshToken: faker.string.sample(),
  resetPass: faker.string.sample(),
};
